//import type { GatsbyConfig } from "gatsby";


module.exports = {
  pathPrefix: `/painting-tools`,
  siteMetadata: {
    title: `Painting Tools`,
    siteUrl: `https://zubak.balazs.gitlab.io/painting-tools/`
  },
  
  plugins: ["gatsby-plugin-styled-components", {
    resolve: 'gatsby-plugin-google-analytics',
    options: {
      "trackingId": "test123toreplace"
    }
  }, "gatsby-plugin-react-helmet", "gatsby-plugin-sitemap", {
    resolve: 'gatsby-plugin-manifest',
    options: {
      "icon": "src/images/icon.png"
    }
  }, "gatsby-plugin-mdx", {
    resolve: 'gatsby-source-filesystem',
    options: {
      "name": "pages",
      "path": "./src/pages/"
    },
    __key: "pages"
  }]
};
