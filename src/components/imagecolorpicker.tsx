import React, {useState, useRef} from "react";
import { Color } from "react-color";
import styled from "styled-components";

function rgb2hex(r:number, g:number, b:number)
{
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);

}

export const ImageColorPicker = (props:{onChange(c:Color):void}) =>
{
    const [tracking, setTracking] = useState<boolean>(false);
    const [pointer, setPointer] = useState<{x:number, y:number}|null>(null);

    let canvas = useRef<HTMLCanvasElement>(null);

    const loadImage = (url:string) =>
    {
        const canv = canvas.current;
        if (!canv) return;
        
        canv.width = canv.offsetWidth;
        canv.height = canv.offsetHeight;
        const context = canv.getContext('2d');

        const img = new Image();
        img.src = url;
        img.crossOrigin = "Anonymous";
        img.onload = () => {
            let wrh = img.width / img.height;
            let newWidth = canv.width;
            let newHeight = newWidth / wrh;
            if (newHeight > canv.height) {
                newHeight = canv.height;
                newWidth = newHeight * wrh;
            }
            context && context.clearRect(0,0, newWidth , newHeight);
            context && context.drawImage(img,0,0, newWidth , newHeight);
        };
    }

    const pickColor = (data) =>
    {
        props.onChange(rgb2hex(data[0], data[1], data[2]));
    }

    const dragOver = (e) => {
        e.preventDefault();
    }
    
    const dragEnter = (e) => {
        e.preventDefault();
    }
    
    const dragLeave = (e) => {
        e.preventDefault();
    }
    
    const fileDrop = (e:any) => {
        e.preventDefault();
        const files = e.dataTransfer.files;


        const reader = new FileReader();
        reader.addEventListener('load', (event) => {
            loadImage(reader.result);
        });
        reader.readAsDataURL(files[0]);

    }
    

    return (
        <StyledImageColorPicker>
            <div className="eye-drop-container">
            <input style={{width:"100%"}} defaultValue="https://st.depositphotos.com/2185383/2281/v/950/depositphotos_22812766-stock-illustration-colorful-background.jpg" onKeyUp={(e) => {if (e.code == "Enter") loadImage(e.target.value)}}/><br/>
            </div>
            <div style={{width:"100%"}}
                onDragOver={dragOver}
                onDragEnter={dragEnter}
                onDragLeave={dragLeave}
                onDrop={fileDrop}
            >
                <ImageCanvas 
                    ref={canvas} 
                    onMouseDown={() => setTracking(true)} 
                    onMouseUp={() => setTracking(false)} 
                    onMouseMove={(e) => {if (tracking) { pickColor(e.target.getContext('2d').getImageData(e.pageX - e.target.offsetLeft, e.pageY - e.target.offsetTop, 1, 1).data)}}} />
                {tracking && <Pointer style={{left:pointer?.x + "px", top:pointer?.y + "px"}} />}
            </div>
        </StyledImageColorPicker>)
}

const StyledImageColorPicker = styled.div`
    
`;
const ImageCanvas = styled.canvas`
    width: 100%;
    height:50vh;
`;

const Pointer = styled.div`
    position: fixed;
    width: 10px;
    height: 10px;
    border: 1px solid #aaaaaa74;
    border-radius: 50%;
    pointer-events: none;
    display:none;
`;