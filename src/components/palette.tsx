import React, {useState, useEffect, useRef} from "react";
import styled from "styled-components";
import { ChromePicker, Color } from "react-color";
import CM, { extendPlugins } from "colormaster";
import MixPlugin from "colormaster/plugins/mix";
import { TFormat } from "colormaster/types";
import { mix } from "../tools/mix";

extendPlugins([MixPlugin]);

export type ColorFactor = {color:Color, factor:number};

const mixColor = (palette:ColorFactor[], colorSpace:TFormat):Color|null =>
{
    let denormalizedPalette = palette.filter(v => v.factor > 0); 
    
    switch (denormalizedPalette.length)
    {
        case 0: return null; break;
        case 1: return CM(denormalizedPalette[0].color).stringHEX({alpha:false}); break;
        default: 
            let first = denormalizedPalette.splice(0,1)[0];
            let curFactor = first.factor;
            return denormalizedPalette.reduce((prev, cur) => { curFactor += cur.factor; return mix( prev, CM(cur.color), cur.factor / curFactor, colorSpace )}, CM(first.color)).stringHEX({alpha:false});
        break;
    };

}

// markup
export const Palette = (props:{defaultPalette:Color[], onChange:(palette:Color[]) => void}) => {
  const [palette, setPalette] = useState<ColorFactor[]>(props.defaultPalette.map(c => {return {color:c, factor:0}}));
  const [colorSpace, setColorSpace] = useState<TFormat>("ryb");

  let mixedColor = mixColor(palette, colorSpace);

  let savePalette = (palette:ColorFactor[]) =>
  {
      let simplePalette = palette.map(cf => cf.color);
      localStorage.setItem("palette", JSON.stringify(simplePalette));
      setPalette(palette);
      props.onChange(simplePalette);
  }

  return (
    <div>
        <AddButton onClick={() => {savePalette(palette.concat({color:"#000000", factor:0}));}}>+</AddButton>
        { palette.map((color:{color:Color,factor:number}, index:number) => 
        <MixPaletteColor key={index} >
            <PaletteColor 
                color={color.color} 
                onRemove={() => {let p = palette.slice(); p.splice(index,1); savePalette(p); } }
                onChange={(c)=> {let p = palette.slice(); p[index] = {color:c, factor:color.factor}; savePalette(p); } } 
            />
            <MixFactor><a onClick={()=> {if (color.factor > 0) {let p = palette.slice(); p[index] = {color:color.color, factor:color.factor-1}; setPalette(p); } }}>-</a> 
            {color.factor} 
            <a onClick={()=> {if (color.factor < 1000) {let p = palette.slice(); p[index] = {color:color.color, factor:color.factor+1}; setPalette(p); } }}>+</a></MixFactor>
        </MixPaletteColor>
         ) }
        <h3>Mixed color</h3>
        Color space: <select onChange={(e) => setColorSpace(e.target.value)} defaultValue={colorSpace}>{["hsl", "hsv", "hwb", "lab", "lch", "luv", "rgb", "ryb", "uvw", "xyz", "cmyk", "hex"].map(c => <option key={c} value={c}>{c}</option>)}</select>
        <div>{ mixedColor && <PaletteColor color={mixedColor} /> }</div>
    </div>
  )
}

export const PaletteColor = (props:{color:Color, onChange?:(e:Color) => void, onRemove?:() => void}) => {
    const [editingColor, setEditingColor] = useState<Color|null>(null);

    const wrapperRef = useRef<HTMLDivElement>(null);

    let handleClickOutside = (event:MouseEvent) => {

        if ( wrapperRef.current && !wrapperRef.current.contains(event.target) )
        {
            setEditingColor(null);
            document.removeEventListener("mousedown", handleClickOutside);
        }
    }
    
    useEffect(() =>
    {
        //return () => {console.log('bye'); document.removeEventListener("mouseup", handleClickOutside); }
    });

    return (
    <StyledPaletteColor>
        <ColorBg color={props.color} onClick={() => { if (props.onChange) { setEditingColor(props.color); document.addEventListener('mousedown', handleClickOutside);} } } />
        {props.onRemove && <Remove onClick={() => {props.onRemove && props.onRemove()}}>x</Remove>}
        <div>{props.color}</div>
        { editingColor && <div ref={wrapperRef} style={{position:"absolute", top: "65px", left: 0, zIndex: 100}} ><ChromePicker color={ editingColor } onChange={ (c) => setEditingColor(c.hex) } onChangeComplete={ (c) => props.onChange(c.hex) } /></div> }
    </StyledPaletteColor>);
}

const MixFactor = styled.div`
    width: 100%;
    text-align: center;
    -moz-user-select: none;
   -khtml-user-select: none;
   -webkit-user-select: none;
    a {
        cursor: pointer;
        padding: 0 10px;
    }
`;

const MixPaletteColor = styled.div`
    display: inline-block;
`;

const StyledPaletteColor = styled.div`
    display: inline-block;
    margin: 10px;
    font-size: 10px;
    text-align: center;
    position: relative;
    border: 1px solid gray;
`;

const ColorBg = styled.div<{color:Color}>`
    width: 100px;
    height: 50px;
    margin: 1px;
    background-color: ${props => props.color};
    box-sizing: content-box;
`;

const AddButton = styled.button`
    width: 104px;
    height: 60px;
    display: block;
    border: 1px solid lightgray;
    background-color: white;
    margin: 10px;
    cursor: pointer;
`;

const Remove = styled.div`
  position: absolute;
  right: -5px;  
  top: -5px;
  border-radius: 50%;
  width: 10px;
  height: 10px;
  background-color: lightgray;
  line-height: 10px;
  font-size: 8px;
  cursor: pointer;
`;