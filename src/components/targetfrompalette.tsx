import React, {useState, useEffect, useRef} from "react";
import styled from "styled-components";
import { ChromePicker, Color } from "react-color";
import { PaletteColor } from "./palette";

const calculateColors = (palette:Color[], targetColor:Color):{color:Color, factor:number}[]|null =>
{
    // FAKE
    return palette[1] ? [{color:palette[0], factor:0.5}, {color:palette[1], factor:0.5}] : null;
}

// markup
export const TargetFromPalette = (props:{palette:Color[], targetColor:Color}) => {

  const paletteFactor = calculateColors(props.palette, props.targetColor);

  return (
    <div>
        { paletteFactor ? paletteFactor.map((result:{color:Color, factor:number}, index:number) => 
        <TargetColor key={index} >
            <PaletteColor 
            color={result.color}  
            />
            <Factor>{result.factor}</Factor>
        </TargetColor> )
        : "No solution from this palette." }
    </div>
  )
}

const TargetColor = styled.div`
    display: inline-block;
`;

const Factor = styled.div`
    text-align: center;
`;
