import React, {useState} from "react";
import { Palette, PaletteColor } from "../components/palette";
import { ChromePicker, Color } from 'react-color'
import { ImageColorPicker } from "../components/imagecolorpicker";
import { Helmet } from "react-helmet";
import { TargetFromPalette } from "../components/targetfrompalette";

// styles
const pageStyles = {
  color: "#232129",
  padding: 96,
  fontFamily: "-apple-system, Roboto, sans-serif, serif",
}



// markup
const Index = () => {
  const [palette, setPalette] = useState<Color[]>((typeof window != "undefined" && JSON.parse(localStorage.getItem("palette") || "[]")) || []);
  const [targetColor, setTargetColor] = useState<Color>("#ffffff");

  return (
    
    <main style={pageStyles}>
      <Helmet>
          <meta charSet="utf-8" />
          <title>Painting Tools</title>
      </Helmet>
      <h2>Palette:</h2>
      <Palette defaultPalette={palette} onChange={(palette => setPalette(palette))} />
      <h2>Determine color of image:</h2>
      <h3>Drag image or load from Url:</h3>
      <ImageColorPicker onChange={(c) => setTargetColor(c)} />
      <h3>Target color:</h3>
      <PaletteColor color={ targetColor } onChange={ (c) => setTargetColor(c) } />
      <h3>Target from palette:</h3>
      <TargetFromPalette palette={palette} targetColor={targetColor}  />
    </main>
  )
}


//<!--<ColorBases targetColor={targetColor} palette={palette} />
export default Index
