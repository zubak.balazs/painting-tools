export type Vector3 = {x:number, y:number, z:number};

export type RGBColor = {r:number, g:number, b:number, a:number};
export type RYBColor = {r:number, y:number, b:number, a:number};

export const VecToCol = (i:Vector3, a:number):RGBColor => {
    return {r:255*i.x, g:255*i.y, b:255*i.z, a:a};
}

export const ColToVec = (c:{r:number, g:number, b:number}) => {
    return {x:c.r/255, y: c.g/255, z:c.b/255};
}

export const Vector3Scale = (v:Vector3, scale:number) => {
    return {x: v.x*scale, y: v.y*scale, z:v.z*scale};
}

export const Vector3Lerp = (v1:Vector3, v2:Vector3, amount:number) => {
    return {x: v1.x + amount*(v2.x-v1.x),
            y: v1.y + amount*(v2.y-v1.y),
            z: v1.z + amount*(v2.z-v1.z)}
}

export const Vector3Add = (v1:Vector3, v2:Vector3):Vector3 => {
	return {x:v1.x+v2.x, y:v1.y+v2.y, z:v1.z+v2.z};
}

export const Saturate = (i:RGBColor, sat:number) => {
    if (Math.abs(sat)<0.004) {return i;}  //Immediately return when sat is zero or so small no difference will result (less than 1/255)
    if ((i.r==0)&&(i.g==0)&&(i.b==0)) {return i;}  //Prevents division by zero trying to saturate black

    let clerp:Vector3={x:i.r/255, y:i.g/255, z:i.b/255};

    if (sat>0.0) {
        let maxsat;
        let mx=Math.max(Math.max(i.r,i.g),i.b);
        mx/=255.0;
        maxsat=Vector3Scale(clerp,1.0/mx);
        clerp=Vector3Lerp(clerp,maxsat,sat);
    }
    if (sat<0.0) {
        let grayc:Vector3;
        let avg:number= (i.r+i.g+i.b);
        avg/=(3.0*255.0);
        grayc={x:avg,y:avg,z:avg};
        clerp=Vector3Lerp(clerp,grayc,-1.0*sat);
    }
    return {r: 255*clerp.x, g:255*clerp.y, b:255*clerp.z, a:255};
}
/*
Color ColorBlindTransform(Color in, int CBtype) {
//Types 0=normal, 1=Protanopia, 2=Deuteranopia, 3=Tritanopia, 4=Achromatopsia
//Matrices taken from https://gist.github.com/Lokno/df7c3bfdc9ad32558bb7

    Color out=BLACK;
    switch(CBtype) {
    case 0:
        return in;
    case 1:
        out.r=in.r*0.567+in.g*0.433+in.b*0.000;
        out.g=in.r*0.558+in.g*0.442+in.b*0.000;
        out.b=in.r*0.000+in.g*0.242+in.b*0.758;
        return out;
    case 2:
        out.r=in.r*0.625+in.g*0.375+in.b*0.000;
        out.g=in.r*0.700+in.g*0.300+in.b*0.000;
        out.b=in.r*0.000+in.g*0.300+in.b*0.700;
        return out;
    case 3:
        out.r=in.r*0.950+in.g*0.050+in.b*0.000;
        out.g=in.r*0.000+in.g*0.433+in.b*0.567;
        out.b=in.r*0.000+in.g*0.475+in.b*0.525;
        return out;
    case 4:
        out.r=in.r*0.299+in.g*0.587+in.b*0.114;
        out.g=in.r*0.299+in.g*0.587+in.b*0.114;
        out.b=in.r*0.299+in.g*0.587+in.b*0.114;
        return out;
    default:
        return in;
    }

return in;
}
*/


//https://github.com/ProfJski/ArtColors/blob/master/RYB.cpp
export const  Xform_RYB2RGB = (color:RYBColor): RGBColor => {
    let rin=color.r/255.0;
    let yin=color.y/255.0;
    let bin=color.b/255.0;


    //The values defined here are where the magic happens.  You can experiment with changing the values and see if you find a better set.  If so, notify me on GitHub @ProfJski !
    //I have included a few alternative sets below

    //RYB corners in RGB values
    //Values arranged to approximate an artist's color wheel
    let CG000={x:0.0,y:0.0,z:0.0}; //Black
    let CG100={x:1.0,y:0.0,z:0.0}; //Red
    let CG010={x:0.9,y:0.9,z:0.0}; //Yellow = RGB Red+Green.  Still a bit high, but helps Yellow compete against Green.  Lower gives murky yellows.
    let CG001={x:0.0,y:0.36,z:1.0}; //Blue: Green boost of 0.36 helps eliminate flatness of spectrum around pure Blue
    let CG011={x:0.0,y:0.9,z:0.2}; //Green: A less intense green than {0,1,0}, which tends to dominate
    let CG110={x:1.0,y:0.6,z:0.0}; //Orange = RGB full Red, 60% Green
    let CG101={x:0.6,y:0.0,z:1.0}; //Purple = 60% Red, full Blue
    let CG111={x:1.0,y:1.0,z:1.0}; //White

/*
    //RYB corners in RGB values
    //Values arranged to approximate an artist's color wheel
    let CG000={x:0.0,y:0.0,z:0.0}; //Black
    let CG100={x:1.0,y:0.0,z:0.0}; //Red
    let CG010={x:0.9,y:0.9,z:0.0}; //Yellow = RGB Red+Green.  Still a bit high, but helps Yellow compete against Green.  Lower gives murky yellows.
    let CG001={x:0.0,y:0.36,z:1.0}; //Blue: Green boost of 0.36 helps eliminate flatness of spectrum around pure Blue
    let CG011={x:0.0,y:0.75,z:0.3}; //Green: A less intense green than {0,1,0}, which tends to dominate
    let CG110={x:1.0,y:0.6,z:0.0}; //Orange = RGB full Red, 60% Green
    let CG101={x:0.6,y:0.0,z:1.0}; //Purple = 60% Red, full Blue
    let CG111={x:1.0,y:1.0,z:1.0}; //White
*/
/*
    //RYB corners in RGB values
    //Unbalanced corners: Less even hue distribution
    let CG000={x:0.0,y:0.0,z:0.0}; //Black
    let CG100={x:1.0,y:0.0,z:0.0}; //Red
    let CG010={x:1.0,y:1.0,z:0.0}; //Yellow
    let CG001={x:0.0,y:0.0,z:1.0}; //Blue:
    let CG011={x:0.0,y:1.0,z:0.0}; //Green:
    let CG110={x:1.0,y:0.5,z:0.0}; //Orange
    let CG101={x:0.5,y:0.0,z:1.0}; //Purple
    let CG111={x:1.0,y:1.0,z:1.0}; //White
*/

    //Trilinear interpolation from RYB to RGB
    let C00,C01,C10,C11;
    C00=Vector3Add(Vector3Scale(CG000,1.0-rin),Vector3Scale(CG100,rin));
    C01=Vector3Add(Vector3Scale(CG001,1.0-rin),Vector3Scale(CG101,rin));
    C10=Vector3Add(Vector3Scale(CG010,1.0-rin),Vector3Scale(CG110,rin));
    C11=Vector3Add(Vector3Scale(CG011,1.0-rin),Vector3Scale(CG111,rin));

    let C0,C1;
    C0=Vector3Add(Vector3Scale(C00,1.0-yin),Vector3Scale(C10,yin));
    C1=Vector3Add(Vector3Scale(C01,1.0-yin),Vector3Scale(C11,yin));

    let C;
    C=Vector3Add(Vector3Scale(C0,1.0-bin),Vector3Scale(C1,bin));

    let CRGB={r:255*C.x,g:255*C.y,b:255*C.z,a:255};

    return CRGB;
}

export const Xform_RGB2RYB = (color:RGBColor):RYBColor => {
    let rin=color.r/255.0;
    let gin=color.g/255.0;
    let bin=color.b/255.0;

    //Finding the appropriate values for the inverse transform was no easy task.  After some experimentation, I wrote a separate program that used
    //the calculus of variations to help tweak my guesses towards values that provided a closer round-trip conversion from RGB to RYB to RGB again.

    //RGB corners in RYB values
    let CG000={x:0.0,y:0.0,z:0.0}; //Black
    let CG100={x:0.891,y:0.0,z:0.0}; //Red
    let CG010={x:0.0,y:0.714,z:0.374}; //Green = RYB Yellow + Blue
    let CG001={x:0.07,y:0.08,z:0.893}; //Blue:
    let CG011={x:0.0,y:0.116,z:0.313}; //Cyan = RYB Green + Blue.  Very dark to make the rest of the function work correctly
    let CG110={x:0.0,y:0.915,z:0.0}; //Yellow
    let CG101={x:0.554,y:0.0,z:0.1}; //Magenta =RYB Red + Blue.  Likewise dark.
    let CG111={x:1.0,y:1.0,z:1.0}; //White

    //Trilinear interpolation from RGB to RYB
    let C00,C01,C10,C11;
    C00=Vector3Add(Vector3Scale(CG000,1.0-rin),Vector3Scale(CG100,rin));
    C01=Vector3Add(Vector3Scale(CG001,1.0-rin),Vector3Scale(CG101,rin));
    C10=Vector3Add(Vector3Scale(CG010,1.0-rin),Vector3Scale(CG110,rin));
    C11=Vector3Add(Vector3Scale(CG011,1.0-rin),Vector3Scale(CG111,rin));

    let C0,C1;
    C0=Vector3Add(Vector3Scale(C00,1.0-gin),Vector3Scale(C10,gin));
    C1=Vector3Add(Vector3Scale(C01,1.0-gin),Vector3Scale(C11,gin));

    let C;
    C=Vector3Add(Vector3Scale(C0,1.0-bin),Vector3Scale(C1,bin));

    let CRYB=Saturate(VecToCol(C, color.a),0.5);

    return {r:CRYB.r, y:CRYB.g, b:CRYB.b, a:CRYB.a};
}


export const ColorMix = (a:RGBColor, b:RGBColor,  blend:number) => {
    let out:RGBColor = {
        r: Math.sqrt((1.0-blend)*(a.r*a.r)+blend*(b.r*b.r)),
        g: Math.sqrt((1.0-blend)*(a.g*a.g)+blend*(b.g*b.g)),
        b: Math.sqrt((1.0-blend)*(a.b*a.b)+blend*(b.b*b.b)),
        a: (1.0-blend)*a.a+blend*b.a 
    }; 
    
    return out;
}

export const ColorMixLin = (a:RGBColor, b:RGBColor, blend:number) => {
    let out:RGBColor = {
        r: (1.0-blend)*a.r+blend*b.r,
        g: (1.0-blend)*a.g+blend*b.g,
        b: (1.0-blend)*a.b+blend*b.b,
        a: (1.0-blend)*a.a+blend*b.a
    }

    return out;
}

export const ColorInv = (i:RGBColor) => {
    return {r: 255-i.r, g:255-i.g, b: 255-i.b, a:255};
}

export const Brighten = (i:RGBColor, bright:number) => {
    let out:RGBColor;
    if (bright==0.0) 
    {
        return i;
    }
    else if (bright>0.0) {
        out=ColorMix(i,{r:255, g:255, b:255, a:255},bright);
    }
    else {
        out=ColorMix(i,{r:0, g:0, b:0, a:0},-1.0*bright);
    }
    return out;
}


export const ColorDistance = (a:RGBColor, b:RGBColor) => {
    let out=((a.r-b.r)*(a.r-b.r)+(a.g-b.g)*(a.g-b.g)+(a.b-b.b)*(a.b-b.b));
    out=Math.sqrt(out)/(Math.sqrt(3.0)*255); //scale to 0-1
    return out;
}

export const ColorMixSub = (a:RGBColor, b:RGBColor, blend:number) => {
    let out:RGBColor;
    let c,d,f:RGBColor;

    c=ColorInv(a);
    d=ColorInv(b);

    f = {
        r: Math.max(0,255-c.r-d.r),
        g: Math.max(0,255-c.g-d.g),
        b: Math.max(0,255-c.b-d.b),
        a: a.a // HACK
    }

    let cd=ColorDistance(a,b);
    cd=4.0*blend*(1.0-blend)*cd;
    out=ColorMixLin(ColorMixLin(a,b,blend),f,cd);

    out.a=255;
    return out;
}
