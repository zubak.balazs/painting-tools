import CM, { ColorMaster, extendPlugins } from "colormaster";
import { TFormat } from "colormaster/types";
import MixPlugin from "colormaster/plugins/mix";
import {rgb2ryb, ryb2rgb, rgb2hsv, hsv2rgb} from "ycolor";
import {ColorMix, ColorMixSub, ColorMixLin, Xform_RGB2RYB, Xform_RYB2RGB} from "./artcolormix";
extendPlugins([MixPlugin]);






/*function rgb2ryb ( iRed:number, iGreen:number, iBlue:number )
{
	// Remove the white from the color
	var iWhite = Math.min( iRed, iGreen, iBlue );
	
	iRed   -= iWhite;
	iGreen -= iWhite;
	iBlue  -= iWhite;
	
	var iMaxGreen = Math.max( iRed, iGreen, iBlue );
	
	// Get the yellow out of the red+green
	
	var iYellow = Math.min( iRed, iGreen );
	
	iRed   -= iYellow;
	iGreen -= iYellow;
	
	// If this unfortunate conversion combines blue and green, then cut each in half to
	// preserve the value's maximum range.
	if (iBlue > 0 && iGreen > 0)
	{
		iBlue  /= 2;
		iGreen /= 2;
	}
	
	// Redistribute the remaining green.
	iYellow += iGreen;
	iBlue   += iGreen;
	
	// Normalize to values.
	var iMaxYellow = Math.max( iRed, iYellow, iBlue );
	
	if ( iMaxYellow > 0 )
	{
		var iN = iMaxGreen / iMaxYellow;
		
		iRed    *= iN;
		iYellow *= iN;
		iBlue   *= iN;
	}
	
	// Add the white back in.
	iRed    += iWhite;
	iYellow += iWhite;
	iBlue   += iWhite;
	
	return [ iRed, iYellow, iBlue ];
}

function ryb2rgb ( iRed:number, iYellow:number, iBlue:number )
{
	// Remove the whiteness from the color.
	var iWhite = Math.min( iRed, iYellow, iBlue );
	
	iRed    -= iWhite;
	iYellow -= iWhite;
	iBlue   -= iWhite;

	var iMaxYellow = Math.max( iRed, iYellow, iBlue );

	// Get the green out of the yellow and blue
	var iGreen = Math.min( iYellow, iBlue );
	
	iYellow -= iGreen;
	iBlue   -= iGreen;

	if ( iBlue > 0 && iGreen > 0 )
	{
		iBlue  *= 2.0;
		iGreen *= 2.0;
	}
	
	// Redistribute the remaining yellow.
	iRed   += iYellow;
	iGreen += iYellow;

	// Normalize to values.
	var iMaxGreen = Math.max( iRed, iGreen, iBlue );
	
	if ( iMaxGreen > 0 )
	{
		var iN = iMaxYellow / iMaxGreen;
		
		iRed   *= iN;
		iGreen *= iN;
		iBlue  *= iN;
	}
	
	// Add the white back in.
	iRed   += iWhite;
	iGreen += iWhite;
	iBlue  += iWhite;

	// Save the RGB	
	return [ iRed, iGreen, iBlue ];
}
*/



const rybMix1a = (color1:ColorMaster, color2:ColorMaster, ratio:number) =>
{
    let c1Ryb = Xform_RGB2RYB({r:color1.red, g:color1.green, b:color1.blue, a:color1.alpha * 255});
    let c2Ryb = Xform_RGB2RYB({r:color2.red, g:color2.green, b:color2.blue, a:color2.alpha * 255});
    console.log(c1Ryb, c2Ryb)
    let rybMix = ColorMix({r:c1Ryb.r, g:c1Ryb.y, b:c1Ryb.b, a:c1Ryb.a}, {r:c2Ryb.r, g:c2Ryb.y, b:c2Ryb.b, a:c2Ryb.a}, ratio);
    console.log(rybMix);
    let mix = Xform_RYB2RGB({r:rybMix.r, y:rybMix.g, b:rybMix.b, a:rybMix.a});
    
    let mixRgb = [mix.r, mix.g, mix.b]
    console.log(mixRgb); 

    return CM({r: mixRgb[0], g: mixRgb[1], b: mixRgb[2] });
}

const rybMix1b = (color1:ColorMaster, color2:ColorMaster, ratio:number) =>
{
    let c1Ryb = Xform_RGB2RYB({r:color1.red, g:color1.green, b:color1.blue, a:color1.alpha * 255});
    let c2Ryb = Xform_RGB2RYB({r:color2.red, g:color2.green, b:color2.blue, a:color2.alpha * 255});
    
    let rybMix = ColorMixLin({r:c1Ryb.r, g:c1Ryb.y, b:c1Ryb.b, a:c1Ryb.a}, {r:c2Ryb.r, g:c2Ryb.y, b:c2Ryb.b, a:c2Ryb.a}, ratio);
    let mix = Xform_RYB2RGB({r:rybMix.r, y:rybMix.g, b:rybMix.b, a:rybMix.a});
    
    let mixRgb = [mix.r, mix.g, mix.b]

    return CM({r: mixRgb[0], g: mixRgb[1], b: mixRgb[2] });
}

/*const rybMix2a = (color1:ColorMaster, color2:ColorMaster, ratio:number) =>
{
    let c1Ryb = rgb2ryb(color1.red , color1.green , color1.blue);
    let c2Ryb = rgb2ryb(color2.red , color2.green , color2.blue);

    let [first, second, third] = c1Ryb.map((_, i) => c1Ryb[i] * (1 - ratio) + c2Ryb[i] * ratio);
    
    let mixRgb = ryb2rgb(first, second, third);
    return CM({r: mixRgb[0] * 255, g: mixRgb[1] * 255, b: mixRgb[2] * 255 });
}

const rybMix2b = (color1:ColorMaster, color2:ColorMaster, ratio:number) =>
{
    let c1Ryb = rgb2ryb(color1.red , color1.green , color1.blue);
    let c2Ryb = rgb2ryb(color2.red , color2.green , color2.blue);

    let [first, second, third] = c1Ryb.map((_, i) => Math.sqrt((1 - ratio) * Math.pow(c1Ryb[i],2) + ratio*Math.pow(c2Ryb[i],2)));
    
    let mixRgb = ryb2rgb(first, second, third);
    return CM({r: mixRgb[0] * 255, g: mixRgb[1] * 255, b: mixRgb[2] * 255 });
}*/

const yColorMix = (color1:ColorMaster, color2:ColorMaster, ratio:number, rgbToMethod:(c:number[]) => number[], rgbFromMethod:(c:number[]) => number[]) =>
{
    let c1Ryb = rgbToMethod([color1.red , color1.green , color1.blue]);
    let c2Ryb = rgbToMethod([color2.red , color2.green , color2.blue]);

    let [first, second, third] = c1Ryb.map((_, i) => c1Ryb[i] * (1 - ratio) + c2Ryb[i] * ratio);
    
    let mixRgb = rgbFromMethod([first, second, third]);
    return CM({r: mixRgb[0], g: mixRgb[1], b: mixRgb[2] });
}



export const mix = (color1:ColorMaster, color2:ColorMaster, ratio:number, colorSpace:TFormat) =>
{
    if (colorSpace == "ryb")
    {
        return yColorMix(color1,color2,ratio, rgb2ryb,ryb2rgb);
    }
    else if (colorSpace == "hsv")
    {
        return yColorMix(color1,color2,ratio, rgb2hsv,hsv2rgb);
    }
    else
    {
        return color1.mix({ color: color2, ratio: ratio, colorspace: colorSpace });
    }
}